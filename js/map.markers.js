function attachMessage(marker, secretMessage) {
	var infowindow = new google.maps.InfoWindow({
		content: secretMessage
	});

	marker.addListener('click', function() {
		infowindow.open(marker.get('map'), marker);
	});
}

function createMarker(map, object) {
	var marker = new google.maps.Marker({
		position: object.position,
		icon: {
			path: google.maps.SymbolPath.CIRCLE,
			scale: object.scale,
			fillColor: 'red',
			fillOpacity: 0.8,
			strokeColor: 'red',
			strokeWeight: 1
		},
		map: map
	});
	attachMessage(marker, object.message);
	return marker;
}

function renderMarkers(map, data) {
	for (i = 0; i < data.length; i++) {
		var marker = createMarker(map, data[i]);
	}
}
