var borderRestrictions = {
	"southLat" : 47.733690,
	"southLng" : 18.287774,
	"northLat" : 49.613712,
	"northLng" : 19.467450,
	"westLat" : 48.382187,
	"westLng" : 16.834226,
	"eastLat" : 49.087080,
	"eastLng" : 22.558573
}

function mapSettings() {
	var mapProp = {
		center:new google.maps.LatLng(48.6930119, 19.7226084),
		zoom: 7,
		streetViewControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

	google.maps.event.addListener(map, 'idle', function(ev) {
		var borders = getBorders(map);
		console.log(borders.northEast.lat());
		var borderPosition = { 
			"northEastLat" : borders.northEast.lat(),
			"northEastLng" : borders.northEast.lng(),
			"southEastLat" : borders.southWest.lat(),
			"southEastLng" : borders.southWest.lng()
		};

		getData(borderPosition);
	});

	renderMarkers(map, fakeData);
}

function getBorders(map) {
	var bounds = map.getBounds();
	var ne = bounds.getNorthEast();
	var sw = bounds.getSouthWest();
	var position = { 
		northEast : ne,
		southWest : sw
	};
	return position;
}

function getData(borders /*, callback*/) {
    $(document).ready(function() {
	    $.ajax({
	        url: 'http://localhost/getData',
	        type: 'post',
	        dataType: 'json',
	        success: function (data) {
	            console.log(data);
	            callback(data);
	        },
	        err: function (err){
	        	console.log(err);
	        },
	        data: borders
	    });
	});
}

google.maps.event.addDomListener(window, 'load', mapSettings);
