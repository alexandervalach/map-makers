
var object = {
	position: {lat : 48.249075, lng : 17.039831},
	scale: 8,
	message: '<b>Mestská časť Bratislava - Záhorská Bystrica</b><br>Obstarávane: Rekonštrukcia nadstavba a prístavba - 1.etapa<br>Možnosť prihlásenia do: <b>25.04.2016 09:00</b><br><a href="https://www.ezakazky.sk/zahorskabystrica/index.cfm?Module=Item&Page=Item&Area=participate&ItemID=44341501">Vian informácii</a>',
	type: '1',
	winner: 1
};

var object2 = {
	position: {lat :48.144776, lng : 17.107874},
	scale: 8,
	message: '<b>Generálny investor Bratislavy</b><br>Obstarávane: Mirbachov palác, pamiatková obnova fasády objektu<br>Možnosť prihlásenia do: <b>29.04.2016 10:00</b><br><a href="https://www.ezakazky.sk/gib/index.cfm?Module=Item&Page=Item&Area=participate&ItemID=20223053">Vian informácii</a>',
	type: '1',
	winner: 2
};

var object3 = {
	position: {lat : 48.258597, lng : 17.276254},
	scale: 8,
	message: '<b>Obec Slovenský Grob</b><br>Obstarávane: Rozšírenie kanalizácie Slovenský Grob<br>Možnosť prihlásenia do: <b>02.05.2016 09:00</b><br><a href="https://www.ezakazky.sk/slovenskygrob/index.cfm?Module=Item&Page=Item&Area=participate&ItemID=42146443">Vian informácii</a>',
	type: '2',
	winner: 3
};

var object4 = {
	position: {lat : 48.719966, lng : 21.251159},
	scale: 8,
	message: '<b>Obecný Úrad Nová Dedina</b><br>Obstarávane: Rekonštrukcia UK v skleníkoch Botanickej záhrady<br>Možnosť prihlásenia do: <b>26.04.2016 08:04</b><br><a href="https://www.ezakazky.sk/index.cfm?Module=Item&Page=ItemDetailUvo&TenderID=11641">Vian informácii</a>',
	type: 'Výroba nábytku',
	winner: 0
};

var object5 = {
	position: {lat : 48.719966, lng : 21.251159},
	scale: 8,
	message: '<b>Univerzita Pavla Jozefa Šafárika v Košiciach</b><br>Obstarávane: Stavebné úpravy<br>Možnosť prihlásenia do: <b>02.05.2016 09:00</b><br><a href="https://www.ezakazky.sk/upjs/index.cfm?Module=Item&Page=Item&ItemID=40449210">Vian informácii</a>',
	type: 'Výroba nábytku',
	winner: 0
};

var fakeData = [object, object2, object3, object4];